DigitalParcel
---

Digital Parcel is a self hosted secure file and license distribution and assignment platform, developed in php with the laravel framework, making it easy to expand or integrate into other systems.

##The development plan

![The plan](https://alexhaslam.me/owncloud/public.php?service=files&t=48c2fc546f64f759520878c89c3b44af&download)

* User accounts
* User management api
* Product / Package Management
* User rights to access products or packages
* Product / Package Download
* Key Generation
* Key Import (for steam keys or keys generated externally)
* Key Assignment (link keys per user with access to a product or package)

## Official Documentation

Documentation is not available yet, but will be here soon.

### Contributing To Digital Parcel

**Please feel free to fork the repository and fix issues. **

### License

Digital Parcel is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)